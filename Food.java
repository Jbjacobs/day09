public class Food
{

    private String description;
    
    /**
     * The getter (or accessor) for the description member
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }
    
    public void setDescription(String inDescription)
    {
        this.description = inDescription;
    }
    
    private int calories;
    
    public int getCalories()
    {
        return calories;
    }
    
    public void setCalories(int inCalories)
    {
        this.calories = inCalories;
    }

    
    
}

